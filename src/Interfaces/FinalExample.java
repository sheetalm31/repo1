package Interfaces;
 class Test
{
  int a= 10;
 
  void m1()
 {
	 a=20;
	 System.out.println(a);
 }
 
}

class Test123 extends Test{
	int a;
	void m1()
	 {
		 a=20;
		 System.out.println("a from Class Test123");
	 }
	
}

public class FinalExample{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Test obj1= new Test();
		Test123 obj2= new Test123();
		
		obj1.m1();
		
		obj2.m1();
	
		
	}

}
