package ExceptionHandling;

public class ExceptionExample {

	public static void main(String[] args) {
		
		System.out.println("Started");
		
		//Thread.sleep(4000); --- checked Exception
		
		int i[]= new int[100];
		i[5]= 100;
		
		
		//System.out.println(0/i); //---- unchecked Exception
		
		
		System.out.println("Stopped");
		

	}

}
