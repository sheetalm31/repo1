package ExceptionHandling;

public class HandlingExceptions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Program Started");
		int a =20;
		
		try 
		{		
		System.out.println(a/0);   //Arithmetic Exceptions
		}
		
		catch(ArithmeticException e)
		{
		System.out.println(e.getMessage());	
		}	
			System.out.println("Program Completed");
		
		
	// ******************************************************************************************************* //	
		
		System.out.println("Program Started");
		
		String s= null;
		
		try
		{
		System.out.println(s.length());
		
		}
		
		catch(Exception e)
		{
			
			System.out.println(e.getMessage());	
		}
		
		
		System.out.println("Program Completed");
		
		

// *********************************************************************************************************//
		
System.out.println("Program Started");
		
		int arr[]= new int[5];
		
		try
		{
		arr[5]=100;
		
		}
		
		catch(Exception e)
		{
			
			System.out.println(e.getMessage());	
			
			
			System.out.println("Program Completed");
		}
		
	
		finally
		{
			System.out.println("Bye");
		}
		
	
		
		
			
		
		


}
}