package Arrays;

public class ArraysForloop {
	
	int a[]= new int[10]; // declaration of array
	
	{
	 a[0]=10;
	 a[1]=20;
	 a[2]=30;
	 a[3]=40;
	 a[4]=50;
}
	 public void print() {
		// for(int i=0; i<=4; i++) {      // using for loop println should be a[i];
		 for(int i:a) {                   // using for each loop
		 System.out.println(i);
	 }
	 }
	
	 
	public static void main(String[] args) {
		
	ArraysForloop obj=new ArraysForloop();
	obj.print();
	
		
		
		
		
		
	
		 
		// TODO Auto-generated method stub

	}

}
