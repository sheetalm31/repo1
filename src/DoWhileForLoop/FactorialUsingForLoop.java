package DoWhileForLoop;

public class FactorialUsingForLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int number = 20;
        long fact = 1;
        for(int i = 1; i <= number; i++)
        {
            fact = fact * i;
        }
        System.out.println("Factorial of "+number+" is: "+fact);
			
		}	
			
	}


