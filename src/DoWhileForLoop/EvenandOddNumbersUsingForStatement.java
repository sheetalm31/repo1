package DoWhileForLoop;

public class EvenandOddNumbersUsingForStatement {

	public static void main(String[] args) {
		
		
		// *** program to find Even NUmbers ***
		
	//	for (int i=2; i<=20; i=i+2) 
	  //  {
		//	System.out.println(i);
		
		 //}
	    
		// *** program to find Odd NUmbers ***
		
		for (int i=1; i<=20; i=i+2) {
		
	    
	    	System.out.println(i);		
		 }
			
		
		
	}

}
