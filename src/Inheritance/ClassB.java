package Inheritance;

public class ClassB extends ClassA
{

	public static void main(String[] args) {
		
		
		// Using object invoking method of ClassA without extends keyword
		/*ClassB obj= new ClassB();
		ClassA obj1= new ClassA();
		obj1.Sum(2, 10);
		System.out.println("Value fron Class A is: ");
    	*/
		
		// Using extends keyword invoking ClassA method in ClassB
		
		ClassB obj2= new ClassB();
		obj2.Sum(2, 10);
		
		//System.out.println("Sum of two number from Class A:");

	}

}
