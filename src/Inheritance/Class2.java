package Inheritance; //for clarity watch Saurabh Thawali

public class Class2 extends Class1 {
	
    public void Lion() {
    	
    	System.out.println("Lion says Roar");
    }
	
    public void Cat() {
    	System.out.println("Cat is Sleeping");
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// inheriting Class1 creating obj of class1 from class2
     /* Class1 obj1= new Class1();
      obj1.Cat();
      
      Class2 obj2= new Class2();
      obj2.Lion();
      */
		
		
		
	// inheriting Class 1 using keyword extends
		
	//	Class2 obj2= new Class2(); // no need of creating obj for class 1, we can use the obj of Class2 to invoke method from Class1
		
	//obj2.Cat();
	//obj2.Lion();
		
		
	//Method Overriding (overriding method Cat in Class1 to Class2)
		Class2 obj2 = new Class2();
		obj2.Cat(); // here it is printing body of Class2 instead of Class1
		
		Class1 obj1= new Class1();
		obj1.Cat(); // created obj of Class 1 to print method Cat, out put will same method from both the classes are print
		
		
		
		
		
		
		
	}

}
