package Inheritance;

public  class Class1 {
	
	protected void Cat() {
    	System.out.println("Cat says Meow");
    }
	
	void Dog() {
		System.out.println("Dog says Uff");
	}
		
	protected void Duck() {
			System.out.println("Duck says Quack Quack");
	
	}

	public void Cow() {
		System.out.println("Cow says Moo Moo");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
     Class1 obj1= new Class1();
     obj1.Dog();
     obj1.Cat();
     obj1.Duck();
     obj1.Cow();
     
     
	}

}
