package Inheritance;

interface One{
	void print();
}

interface Two{
	void print1();
}

public class InterfaceExample implements One,Two {
   
	
	public void print() {System.out.println("Hello");}
	public void print1() {System.out.println("Bye");}
	
	public static void main(String[] args) {
		
		One obj= new InterfaceExample();
		obj.print();
		

	}
	

}
