package Inheritance;

// import Arrays.*;// all the classes and methods from package Arrays are printed

// import Arrays.ArraysForloop; // class "Arrays" from package Arrays are printed

import Arrays.*;

public  class Class {

     public static void main(String[] args) {
	
		
		Class obj1= new Class();
	  //  obj1.Dog(); // default access modifier so can be accessed in same package
	 //   obj1.Cat(); // method cat is set to private so cannot be accessed in same  package
	//	  obj1.Duck(); // method is invoked using inheritance since it is protected in Class1
		
			}


}