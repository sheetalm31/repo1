package Oops;

//creating class/method/objects

public class EmployeeUsingObject 
{
int EmpID;//data member or variable
String EmpName;
int Salary;
int DeptNUm;


 void display() // display method
{	
	System.out.println(EmpID);
	System.out.println(EmpName);
	System.out.println(Salary);
	System.out.println(DeptNUm);
}

void Salary() // display salary of Employee
{
	System.out.println(Salary);

}

void EmpID() 
{
	System.out.println(EmpID);
}

public static void main(String args[]) {
	
	// Assigning values to class variables using object
	
	
	EmployeeUsingObject Emp1=new EmployeeUsingObject();
	Emp1.EmpID= 10;
	Emp1.EmpName= "Bob";
	Emp1.Salary= 10000;
	Emp1.DeptNUm= 20;
	// Emp1.display();
	// Emp1.EmpID(); 
    
    EmployeeUsingObject Emp2=new EmployeeUsingObject();
    Emp2.EmpID= 30;
	Emp2.EmpName= "jack";
	Emp2.Salary= 20000;
	Emp2.DeptNUm= 40;
	// Emp2.display();
	// Emp2.Salary();
	
	
} 

}