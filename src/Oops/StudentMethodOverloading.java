package Oops;

public class StudentMethodOverloading {
	
	void sum(int a,float  b )
	{
		System.out.println(a+b);
	}
	
	void sum(int a , int b, int c)
	{
		System.out.println(a+b+c);
	}
	
	
	public static void main(String[] args) {
		StudentMethodOverloading S1= new StudentMethodOverloading();
		S1.sum(2, 3);
		S1.sum(2,3,5);
		//S1.sum(2, 3.5f);
		
		

		
	}

}
