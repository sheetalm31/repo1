package Oops;

public class StudentUsingObject {
	
	int StudentID;
	String StudentName;
	static int StudentMarks;
	int ClassNo;
	
	void display() {
		System.out.println(StudentID);
		System.out.println("StudentName");
		System.out.println(StudentMarks);
		System.out.println(ClassNo);
	}

	static void marks() {
		
		System.out.println("Here are the marks secured:"+StudentMarks);
	}
	
	public static void main(String[] args) {
		
		StudentUsingObject Stu1= new StudentUsingObject();
		{
			Stu1.StudentID= 10;
			Stu1.StudentName= "Alex";
			Stu1.StudentMarks= 40;
			Stu1.ClassNo = 101;
		}

	StudentUsingObject Stu2 = new StudentUsingObject();
	
	{
		Stu2.StudentID=20;
		Stu2.StudentName= "Sam";
        Stu2.StudentMarks=0;
		Stu2.ClassNo= 103;
	}
	
	Stu1.display();
	StudentUsingObject.marks();// invoking marks method using class since it is static
	
	
	
	}

}
