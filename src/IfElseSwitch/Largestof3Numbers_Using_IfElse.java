package IfElseSwitch;

// Program to find largest among 3 numbers using if .. else program

public class Largestof3Numbers_Using_IfElse {

	public static void main(String[] args) {
		
		int A=100; int B= 200; int C= 300;
		
		if ((A>B) && (A>C)) {
			
			System.out.println("A is the largest among A, B and C");	
		}
		
		else if((B>A) && (B>C)) {
			
			System.out.println("B is the largest among A, B and C");

		}
		else {
			System.out.println("C is the largest among A, B and C");
		
		}	
		
	}

}







